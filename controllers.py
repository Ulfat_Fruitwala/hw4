"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""

from py4web import action, request, abort, redirect, URL, Field
from yatl.helpers import A
from .common import db, session, T, cache, auth, logger, authenticated, unauthenticated, flash
from py4web.utils.url_signer import URLSigner
from .models import get_user_email
from py4web.utils.form import Form, FormStyleBulma
from pydal.validators import *

url_signer = URLSigner(session)

@action('index')
@action.uses(db, auth.user, 'index.html')
def index():
    rows=db(db.contact.user_email == get_user_email()).select().as_list()
    for r in rows:
        s = ''
        numbers = db(db.phone.contact_id == r["id"]).select()
        for n in numbers:
            s = s + n.number + '(' + n.type + '), '
        r["phone"] = s
    return dict(rows=rows, url_signer=url_signer)

@action('add_contact', method=["GET", "POST"])
@action.uses(db, auth.user, 'add_contact.html')
def add_contact():
    form = Form([Field('first_name'), Field('last_name')], csrf_session=session, formstyle=FormStyleBulma)
    # = Form(db.contact, csrf_session=session, formstyle=FormStyleBulma)
    if form.accepted:
        db.contact.insert(first_name = form.vars['first_name'], last_name = form.vars['last_name'])
        redirect(URL('index'))
    return dict(form=form)

@action('edit_contact/<contact_id:int>', method=["GET", "POST"])
@action.uses(db, session, auth.user, 'edit_contact.html')
def edit_contact(contact_id=None):
    assert contact_id is not None
    p = db.contact[contact_id]
    if p is None:
        redirect(URL('index'))
    form = Form([Field('first_name'), Field('last_name')],record=p, deletable=False, csrf_session=session, formstyle=FormStyleBulma)
    #form = Form(db.contact, record=p, deletable=False, csrf_session=session, formstyle=FormStyleBulma)
    if form.accepted:
        db(db.contact.id==contact_id).update(first_name=form.vars['first_name'], last_name=form.vars['last_name'])
        redirect(URL('index'))
    return dict(form=form)

@action('delete_contact/<contact_id:int>')
@action.uses(db, session, auth.user, url_signer.verify())
def delete_contact(contact_id=None):
    assert contact_id is not None
    db(db.contact.id == contact_id).delete()
    redirect(URL('index'))

@action('phone_index/<contact_id:int>', method=["GET", "POST"])
@action.uses(db, session, auth.user, 'phone_index.html')
def phone_index(contact_id=None):
    assert contact_id is not None
    p=db.contact[contact_id]
    if p is not None:
        rows=db(db.phone.contact_id == contact_id).select()
    name=p.first_name + ' ' + p.last_name + '\'s'
    return dict(rows=rows, name=name, contact_id=contact_id, url_signer=url_signer)

@action('add_number/<contact_id:int>', method=["GET", "POST"])
@action.uses(db, session, auth.user, 'add_number.html')
def add_number(contact_id=None):
    assert contact_id is not None
    form = Form([Field('number'), Field('type')], csrf_session=session, formstyle=FormStyleBulma)
    if form.accepted:
        db.phone.insert(number=form.vars['number'], type=form.vars['type'], contact_id=contact_id)
        redirect(URL('phone_index',contact_id))
    return dict(form=form)

@action('edit_number/<number_id:int>', method=["GET", "POST"])
@action.uses(db, session, auth.user, 'edit_number.html')
def edit_number(number_id=None):
    assert number_id is not None
    n=db.phone[number_id]
    if n is None:
        redirect(URL('index'))
    form = Form([Field('number'), Field('type')], record=n, deletable=False, csrf_session=session, formstyle=FormStyleBulma)
    if form.accepted:
        db(db.phone.id == number_id).update(number=form.vars['number'], type=form.vars['type'])
        contact_id = db.phone[number_id].contact_id
        redirect(URL('phone_index', contact_id))
    return dict(form=form)

@action('delete_number/<number_id:int>')
@action.uses(db, session, auth.user, url_signer.verify())
def delete_number(number_id=None):
    assert number_id is not None
    contact_id = db.phone[number_id].contact_id
    db(db.phone.id == number_id).delete()
    redirect(URL('phone_index', contact_id))
