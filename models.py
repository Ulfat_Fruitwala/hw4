"""
This file defines the database models
"""

import datetime
from .common import db, Field, auth
from pydal.validators import *


def get_user_email():
    return auth.current_user.get('email') if auth.current_user else None

def get_time():
    return datetime.datetime.utcnow()

db.define_table(
    'contact',
    Field('first_name'),
    Field('last_name'),
    Field('phone'),
    Field('user_email', default=get_user_email),
)

db.define_table(
    'phone',
    Field('contact_id', 'reference contact'),
    Field('number'),
    Field('type'),
)

db.contact.id.readable = False
db.contact.user_email.readable = False
db.contact.user_email.writable = False
db.phone.id.readable = False

db.commit()

### Define your table below
#
# db.define_table('thing', Field('name'))
#
## always commit your models to avoid problems later

db.commit()
